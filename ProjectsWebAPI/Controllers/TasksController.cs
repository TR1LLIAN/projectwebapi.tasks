﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet]
        public async Task<ActionResult<List<TaskDTO>>> GetAsync()
        {
            return Ok(await _taskService.GetListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetByIdAsync(int id)
        {
            return Ok(await _taskService.GetByIdAsync(id));
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _taskService.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] TaskDTO task)
        {
            try
            {
                await _taskService.CreateAsync(task);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPut]
        public async Task<ActionResult> PutAsync(int id, [FromBody] TaskDTO task)
        {
            try
            {
                await _taskService.UpdateAsync(id, task);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpGet("UnfinishedTasks/{id}")]
        public ActionResult<TaskDTO> GetUnfinishedTasksForUser(int id)
        {
            List<TaskDTO> result;

            try
            {
                result = _taskService.GetUnfinishedTasks(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            if (result.Count == 0)
                return NotFound();

            return Ok(result);
        }
    }
}
