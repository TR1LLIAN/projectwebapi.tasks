﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<List<TeamDTO>>> GetAsync()
        {
            return Ok(await _teamService.GetListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetByIdAsync(int id)
        {
            return Ok(await _teamService.GetByIdAsync(id));
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _teamService.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] TeamDTO team)
        {
            try
            {
                await _teamService.CreateAsync(team);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> PutAsync(int id, [FromBody] TeamDTO team)
        {
            try
            {
                await _teamService.UpdateAsync(id, team);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }
    }
}
