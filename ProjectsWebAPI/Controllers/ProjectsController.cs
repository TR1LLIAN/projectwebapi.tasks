﻿
using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        public async Task<ActionResult<List<ProjectDTO>>> GetAsync()
        {
            return Ok(await _projectService.GetListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetByIdAsync(int id)
        {
            return Ok(await _projectService.GetByIdAsync(id));
        }

        [Route("{id}")]
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            try
            {
                _projectService.Delete(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return Ok();
        }

        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO project)
        {
            try
            {
                _projectService.Create(project);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return Ok();
        }

        [HttpPut]
        public ActionResult Put(int id, [FromBody] ProjectDTO project)
        {
            try
            {
                _projectService.Update(id, project);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }
    }
}
