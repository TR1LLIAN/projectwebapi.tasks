﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserDTO>>> GetAsync()
        {
            return Ok(await _userService.GetUsersAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetByIdAsync(int id)
        {
            return Ok(await _userService.GetByIdAsync(id));
        }


        [Route("{id}")]
        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _userService.DeleteAsync(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] UserDTO user)
        {
            try
            {
                await _userService.CreateAsync(user);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return NotFound();
            }
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> PutAsync(int id, [FromBody] UserDTO user)
        {
            try
            {
                await _userService.UpdateAsync(id, user);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest();
            }
            return NoContent();
        }

    }
}
