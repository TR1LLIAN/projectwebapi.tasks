﻿
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Repositories;
using System.Threading.Tasks;

namespace Projects.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectDbContext _projectWebApiContext;


        public UnitOfWork(ProjectDbContext projectDbcontext)
        {
            _projectWebApiContext = projectDbcontext;

            if (Users == null) Users = new UserRepository(_projectWebApiContext);
            if (Projects == null) Projects = new ProjectRepository(_projectWebApiContext);
            if (Tasks == null) Tasks = new TaskRepository(_projectWebApiContext);
            if (Teams == null) Teams = new TeamRepository(_projectWebApiContext);

        }

        public void Save()
        {
            _projectWebApiContext.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _projectWebApiContext.SaveChangesAsync();
        }

        public IUserRepository Users { get; }

        public ITaskRepository Tasks { get; }

        public ITeamRepository Teams { get; }

        public IProjectRepository Projects { get; }
    }
}
