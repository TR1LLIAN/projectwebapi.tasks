﻿using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;

namespace Projects.DAL.Models
{
    public class TeamModel:BaseModel
    {
        public List<UserDTO> TeamPerformes { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
