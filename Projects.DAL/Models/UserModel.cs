﻿using Projects.Common.DTOmodels;
using System.Collections.Generic;

namespace Projects.DAL.Models
{
    public class UserModel
    {
        public UserDTO User { get; set; }
        public List<TaskModel>? UnFinishedTasks { get; set; }
        public ProjectDTO? LastProject { get; set; }
        public TaskModel? LongestTask { get; set; }
    }
}
