﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Projects.DAL.Context
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options)
        {

        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
               .HasMany(u => u.Projects)
               .WithOne(pr => pr.User)
               .HasForeignKey(pr => pr.AuthorId)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
               .HasMany(u => u.Tasks)
               .WithOne(u => u.User)
               .HasForeignKey(t => t.PerformerId)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Project>()
            .Property(p => p.Id)
            .ValueGeneratedOnAdd().UseIdentityColumn(); 

            modelBuilder.Entity<Task>()
            .Property(p => p.Id)
            .ValueGeneratedOnAdd().UseIdentityColumn(); 

            modelBuilder.Entity<Team>()
            .Property(p => p.Id)
            .ValueGeneratedOnAdd().UseIdentityColumn();

            modelBuilder.Entity<User>()
            .Property(p => p.Id)
            .ValueGeneratedOnAdd().UseIdentityColumn();

            var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("JsonFiles\\teams.json"));
            teams.ForEach(x => { x.Id += 1; });
            modelBuilder.Entity<Team>().HasData(teams);


            var tasks = JsonConvert.DeserializeObject<List<Models.Task>>(File.ReadAllText("JsonFiles\\tasks.json"));
            tasks.ForEach(x => { x.Id += 1; x.PerformerId += 1; x.ProjectId += 1; });
            modelBuilder.Entity<Task>().HasData(tasks);
            

            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("JsonFiles\\users.json"));
            users.ForEach(x => { x.Id += 1; x.Name = x.FirstName; x.TeamId += 1; });
            modelBuilder.Entity<User>().HasData(users);


            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("JsonFiles\\projects.json"));
            projects.ForEach(x => { x.Id += 1; x.AuthorId += 1; x.TeamId += 1; });
            modelBuilder.Entity<Project>().HasData(projects);
            



        }
    }
}
