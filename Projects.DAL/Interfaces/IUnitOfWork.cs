﻿using System.Threading.Tasks;

namespace Projects.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }
        ITaskRepository Tasks { get; }
        ITeamRepository Teams { get; }
        IProjectRepository Projects { get; }
        void Save();
        Task SaveAsync();
    }
}
