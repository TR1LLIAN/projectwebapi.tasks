﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.DAL.Interfaces
{
    public interface IRepository<T> where T:class    
    {
        List<T> GetList();
        T GetItemById(int id);

        Task<List<T>> GetListAsync();
        Task<T> GetItemByIdAsync(int id);

        void Create(T item);
        Task CreateAsync(T item);

        void Update(int id, T item);
        Task UpdateAsync(int id, T item);

        void Update(T item);

        void Delete(int id);
        Task DeleteAsync(int id);
    }
}
