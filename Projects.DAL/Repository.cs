﻿using Microsoft.EntityFrameworkCore;
using Projects.DAL.Context;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.DAL
{
    public class Repository<T> : IRepository<T> where T : BaseModel
    {
        private readonly ProjectDbContext _items;

        public Repository(ProjectDbContext items)
        {
            _items = items;
        }

        public void Create(T item)
        {
            if (_items.Set<T>().Contains(item))
            {
                throw new InvalidOperationException("Such entry already exist!");
            }
            else
            {
                _items.Set<T>().Add(item);
                _items.SaveChanges();
            }
        }

        public async System.Threading.Tasks.Task CreateAsync(T item)
        {
            if (_items.Set<T>().Contains(item))
            {
                throw new InvalidOperationException("Such entry already exist!");
            }
            else
            {
                await _items.Set<T>().AddAsync(item);
            }
        }

        public void Delete(int id)
        {
            T deletedItem = _items.Set<T>().FirstOrDefault(it => it.Id == id);
            if(deletedItem is null)
            {
                throw new InvalidOperationException("Cannot delete not existing item!");
            }
            _items.Set<T>().Remove(deletedItem);
            _items.SaveChanges();
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            T deletedItem = await _items.Set<T>().FirstOrDefaultAsync(it => it.Id == id);
            if (deletedItem is null)
            {
                throw new InvalidOperationException("Cannot delete not existing item!");
            }
            _items.Set<T>().Remove(deletedItem);
            _ = await _items.SaveChangesAsync();
        }

            public T GetItemById(int id)
        {
            return _items.Set<T>().AsNoTracking().FirstOrDefault(it => it.Id == id);
        }

        public Task<T> GetItemByIdAsync(int id)
        {
            return _items.Set<T>().AsNoTracking().FirstOrDefaultAsync(it => it.Id == id);
        }

        public List<T> GetList()
        {
            return _items.Set<T>().AsNoTracking().ToList();
        }

        public async Task<List<T>> GetListAsync()
        {
            return await _items.Set<T>().AsNoTracking().ToListAsync();
        }

        public void Update(int id,T item)
        {
            if (_items.Set<T>().Any(x=>x.Id == id))
            {
                item.Id = id;
                _items.Set<T>().Update(item).State = EntityState.Modified;
            }
            _items.SaveChanges();
        }

        public void Update(T item)
        {
            _items.Entry(item).State = EntityState.Modified;
        }

        public async System.Threading.Tasks.Task UpdateAsync(int id, T item)
        {
            if (_items.Set<T>().Any(x => x.Id == id))
            {
                item.Id = id;
                _items.Set<T>().Update(item).State = EntityState.Modified;
            }
            await _items.SaveChangesAsync();
        }
    }
}
