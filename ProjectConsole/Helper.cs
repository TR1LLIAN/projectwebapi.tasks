﻿using System;
using System.Drawing;
using System.Threading;

namespace ProjectConsole
{
    public static class Helper
    {
        public static readonly string MainMenu = "Select menu item from 1 to 9 \n " +
           "1) Get user tasks per Project \n " +
           "2) Get list of tasks for certain user by Id where task name < 45 \n " +
           "3) Get List (id,name) of tasks which were finished in 2021 for user by Id \n " +
           "4) Get List of teams \n with performers older then 10 years grouped by registrations date and team \n " +
           "5) Get List of users with tasks grouped by name descending  \n " +
           "6) Get User and his last project, number of tasks for this project, total amount not finished tasks \n " +
           "7) Get List of projects with longest description task + shortest name task \n + amount of performers if description of task > 20 or amount of tasks is less then 3 \n " +
           "8) CRUD operations \n " +
           "9) Start random task finisher (^_^) \n " +
           "0) Exit";

        public static readonly string CrudMenu = "CRUD operation for: \n" +
            " 1)Users \n" +
            " 2)Projects \n" +
            " 3)Teams \n" +
            " 4)Tasks \n" +
            " 5)Exit! :) \n";

        public static readonly string CrudOperations = "CRUD operations : \n" +
            " 1)Create \n" +
            " 2)Delete \n" +
            " 3)Update \n" +
            " 4)Find by the ID \n" +
            " 5)Show all \n" +
            " 6)Exit! :) \n";

        public static string CutString(string str, int n)
        {
            if (str.Length < n)
            {
                return str;
            }
            return str.Substring(0, str.Length < n ? str.Length : n) + "...";
        }

        public static void MultiLineAnimation()
        {
            var counter = 0;
            for (int i = 0; i < 6; i++)
            {
                Console.Clear();
                Console.WriteLine("Please wait, loading data :-)...");
                switch (counter % 4)
                {
                    case 0:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║    │││ \\   ║");
                            Console.WriteLine("║    │││  O  ║");
                            Console.WriteLine("║    OOO     ║");
                            break;
                        };
                    case 1:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    OOOO    ║");
                            break;
                        };
                    case 2:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║   / │││    ║");
                            Console.WriteLine("║  O  │││    ║");
                            Console.WriteLine("║     OOO    ║");
                            break;
                        };
                    case 3:
                        {
                            Console.WriteLine("╔════╤╤╤╤════╗");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    ││││    ║");
                            Console.WriteLine("║    OOOO    ║");
                            break;
                        };
                }

                counter++;
                Thread.Sleep(200);
            }
        }

        public static void ColorfulAnimation()
        {
            for (int i = 0; i < 1; i++)
            {
                for (int j = 0; j < 18; j++)
                {
                Console.Clear();
                Console.WriteLine("Please wait a moment...");
                Console.Write("                . . . . o o o o o o", Color.LightGray);
                for (int s = 0; s < j; s++)
                {
                    Console.Write(" o", Color.LightGray);
                }
                Console.WriteLine();

                var margin = "".PadLeft(2 * j);
                Console.Write(" me "); Console.WriteLine(margin + "                    ______    o", Color.LightGray);
                Console.Write(" o "); Console.WriteLine(margin + "   ______===________  ]OO|_n_n__][.", Color.DeepSkyBlue);
                Console.Write("/O\\"); Console.WriteLine(margin + "  [_Binary_Academy__]_|__|________)<| ", Color.DeepSkyBlue);
                Console.Write("/ \\"); Console.WriteLine(margin + "   oo    oo    oo  oo  oo OOOO-| oo\\_", Color.Blue);
                Console.WriteLine("   +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+", Color.Silver);

                Thread.Sleep(110);
            }
        }
    }

}
}
