﻿using ProjectConsole.Services;
using System;
using System.Threading.Tasks;
using System.Timers;

namespace ProjectConsole
{
    public class TaskKiller
    {
        private readonly TaskService _service;

        public TaskKiller()
        {
            _service = new TaskService();
        }

        public Task<int?> MarkRandomTaskWithDelay(int delay)
        {
            TaskCompletionSource<int?> tcs = new TaskCompletionSource<int?>();
            System.Timers.Timer timer = new Timer(delay);

            async void eventHandler(object o, ElapsedEventArgs args)
            {
                timer.Elapsed -= eventHandler;
                var tasks = await _service.ShowTasksAsync().ConfigureAwait(false);
                Random rdm = new Random();
                var task = tasks[rdm.Next(0, tasks.Count)];
                if (task.State == 2)
                {
                    tcs.SetException(new Exception($"This task {task.Id} already marked as finished "));
                }
                else
                {
                    task.State = 2;
                    await _service.UpdateTaskAsync(task).ConfigureAwait(false);
                    tcs.SetResult(task.Id);
                }
            }

            timer.Start();
            timer.AutoReset = true;
            timer.Elapsed += eventHandler;
            return tcs.Task;
        }
    }
}
