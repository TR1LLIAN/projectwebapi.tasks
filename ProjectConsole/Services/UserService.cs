﻿using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProjectConsole.Services
{
    public class UserService
    {
        private readonly HttpClient _client = new HttpClient();
        const string Alias = "https://localhost:44320/api/Users/";


        public async Task<string> AdduserAsync()
        {
            Console.WriteLine("Enter user FirstName: ");
            UserDTO user = new UserDTO
            {
                FirstName = Console.ReadLine()
            };
            Console.Write("Enter Last name: ");
            user.LastName = Console.ReadLine();

            Console.Write("Enter e-mail: ");
            user.Email = Console.ReadLine();

            user.RegisteredAt = DateTime.Now;
            HttpResponseMessage response = await _client.PostAsync(Alias, new StringContent(System.Text.Json.JsonSerializer.Serialize(user), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            return temp;

        }

        public async Task<bool> UpdateUserAsync()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            UserDTO user = new UserDTO();
            Console.WriteLine("Enter user Name: ");
            user.FirstName = Console.ReadLine();

            Console.Write("Enter Last name: ");
            user.LastName = Console.ReadLine();

            Console.Write("Enter e-mail: ");
            user.Email = Console.ReadLine();

            Console.WriteLine("Enter team id");
            user.TeamId = Convert.ToInt32(Console.ReadLine());


            string json = JsonConvert.SerializeObject(user);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync(Alias + $"?{id}", data);
            return response.IsSuccessStatusCode;
        }

        internal async Task<List<UserDTO>> ShowUsersAsync()
        {
            HttpResponseMessage response = await _client.GetAsync(Alias);
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<UserDTO> users = JsonConvert.DeserializeObject<List<UserDTO>>(temp);
            return users;
        }

        public async Task FindUserById()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.GetAsync(Alias+$"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            UserDTO user = JsonConvert.DeserializeObject<UserDTO>(temp);
            if (user != null)
            {
                Console.WriteLine(temp);
            }

        }

        internal async Task DeleteUserAsync()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.DeleteAsync(Alias + $"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            UserDTO user = JsonConvert.DeserializeObject<UserDTO>(temp);
            if (user != null)
            {
                Console.WriteLine(temp);
            }
        }
    }
}
