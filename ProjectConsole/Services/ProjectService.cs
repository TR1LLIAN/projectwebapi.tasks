﻿using Newtonsoft.Json;
using Projects.Common.DTOmodels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectConsole.Services
{
    public class ProjectService
    {
        private readonly HttpClient _client = new HttpClient();
        const string Alias = "https://localhost:44320/api/Projects/";
        public async Task<string> AddProjectAsync()
        {
            Console.WriteLine("Enter project Name: ");
            ProjectDTO project = new ProjectDTO();
            project.Name = Console.ReadLine();
            Console.Write("Enter project description: ");
            project.Description = Console.ReadLine();

            project.CreatedAt = DateTime.Now;

            Console.Write("Enter Author ID: ");
            project.AuthorId =Convert.ToInt32( Console.ReadLine());

            Console.Write("Enter deadline: ");
            project.Deadline = Convert.ToDateTime(Console.ReadLine());

            Console.Write("Enter teamID: ");
            project.TeamId = Convert.ToInt32(Console.ReadLine());
  
            HttpResponseMessage response = await _client.PostAsync(Alias, new StringContent(System.Text.Json.JsonSerializer.Serialize(project), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            return temp;

        }


        public async Task<bool> UpdateProjectAsync()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            ProjectDTO project = new ProjectDTO();

            Console.WriteLine("Enter project Name: ");
            project.Name = Console.ReadLine();

            Console.Write("Enter project description: ");
            project.Description = Console.ReadLine();

            Console.Write("Enter Author ID: ");
            project.AuthorId = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter deadline: ");
            project.Deadline = Convert.ToDateTime(Console.ReadLine());

            Console.Write("Enter teamID: ");
            project.TeamId = Convert.ToInt32(Console.ReadLine());


            string json = JsonConvert.SerializeObject(project);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync(Alias + $"?{id}", data);
            return response.IsSuccessStatusCode;
        }

        internal async Task<List<ProjectDTO>> ShowProjectsAsync()
        {
            HttpResponseMessage response = await _client.GetAsync(Alias);
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<ProjectDTO> projects = JsonConvert.DeserializeObject<List<ProjectDTO>>(temp);
            return projects;
        }

        public async Task FindUserById()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.GetAsync(Alias + $"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            ProjectDTO project = JsonConvert.DeserializeObject<ProjectDTO>(temp);
            if (project != null)
            {
                Console.WriteLine(temp);
            }
        }

        internal async Task DeleteProjectAsync()
        {
            Console.WriteLine("Enter ID: ");
            int id = Convert.ToInt32(Console.ReadLine());
            HttpResponseMessage response = await _client.DeleteAsync(Alias + $"{id}");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            ProjectDTO project = JsonConvert.DeserializeObject<ProjectDTO>(temp);
            if (project != null)
            {
                Console.WriteLine(temp);
            }
        }
    }
}
