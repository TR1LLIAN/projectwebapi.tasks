﻿using AutoMapper;
using Projects.Common.DTOmodels;
using Projects.DAL.Models;

namespace Projects.BLL.Maping
{
    public class MapProject : Profile
    {
        public MapProject()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>().ForMember(src => src.Id, opts => opts.Ignore());
        }
    }
}
