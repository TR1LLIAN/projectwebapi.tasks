﻿using AutoMapper;
using Projects.Common.DTOmodels;
using Projects.DAL.Models;

namespace Projects.BLL.Maping
{
    public class MapTask:Profile
    {
        public MapTask()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<TaskDTO, Task>().ForMember(src => src.Id, opts => opts.Ignore());
        }
    }
}
