﻿using Projects.Common.DTOmodels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Projects.BLL.Interfaces
{
    public interface IUserService:IGenericService<UserDTO>
    {
        List<UserDTO> GetUsers();
        Task<List<UserDTO>> GetUsersAsync();

        void Delete(int id);
        Task DeleteAsync(int id);

        void Create(UserDTO user);
        Task CreateAsync(UserDTO user);

        void Update(int id,UserDTO user);
        Task UpdateAsync(int id, UserDTO user);

        void Update(UserDTO user);

        UserDTO GetById(int id);
        Task<UserDTO> GetByIdAsync(int id);
    }
}
