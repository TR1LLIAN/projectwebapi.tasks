﻿using Projects.Common.DTOmodels;
using Projects.DAL.Models;
using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface ITaskService:IGenericService<TaskDTO>
    {
        List<TaskDTO> GetTasks();
        void Delete(int id);
        void Create(TaskDTO task);
        void Update(int id,TaskDTO task);
        void Update(TaskDTO task);
        TaskDTO GetById(int id);
        List<TaskDTO> GetUnfinishedTasks(int userId);
    }
}
