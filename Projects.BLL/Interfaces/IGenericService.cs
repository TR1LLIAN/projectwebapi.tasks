﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projects.BLL.Interfaces
{
    public interface IGenericService<T> where T:class
    {
        Task<List<T>> GetListAsync();

        void Delete(int id);
        Task DeleteAsync(int id);

        void Create(T item);
        Task CreateAsync(T item);

        void Update(int id, T item);
        Task UpdateAsync(int id, T item);

        void Update(T item);

        T GetById(int id);
        Task<T> GetByIdAsync(int id);
    }
}
