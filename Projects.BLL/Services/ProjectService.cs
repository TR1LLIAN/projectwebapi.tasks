﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(ProjectDTO project)
        {
            if (project.TeamId != null)
                _ = _unitOfWork.Teams.GetItemById((int)project.TeamId) ?? throw new ArgumentException($"Team with Id {project.TeamId} doesn't exist!");
            if (project.AuthorId != null)
                _ = _unitOfWork.Users.GetItemById((int)project.AuthorId) ?? throw new ArgumentException($"User with Id {project.AuthorId} doesn't exist!");

            _unitOfWork.Projects.Create(_mapper.Map<Project>(project));
        }

        public async System.Threading.Tasks.Task CreateAsync(ProjectDTO project)
        {
            if (project.TeamId != null)
                _ =await _unitOfWork.Teams.GetItemByIdAsync((int)project.TeamId) ?? throw new ArgumentException($"Team with Id {project.TeamId} doesn't exist!");
            if (project.AuthorId != null)
                _ =await _unitOfWork.Users.GetItemByIdAsync((int)project.AuthorId) ?? throw new ArgumentException($"User with Id {project.AuthorId} doesn't exist!");

            await _unitOfWork.Projects.CreateAsync(_mapper.Map<Project>(project));
        }

        public void Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            await _unitOfWork.Projects.DeleteAsync(id);
        }

        public ProjectDTO GetById(int id)
        {

            return _mapper.Map<ProjectDTO>(_unitOfWork.Projects.GetItemById(id));

        }

        public async Task<ProjectDTO> GetByIdAsync(int id)
        {
            return _mapper.Map<ProjectDTO>(await _unitOfWork.Projects.GetItemByIdAsync(id));
        }

        public async Task<List<ProjectDTO>> GetListAsync()
        {
            return _mapper.Map<List<ProjectDTO>>(await _unitOfWork.Projects.GetListAsync());
        }

        public List<ProjectDTO> GetProjects()
        {
            return _mapper.Map<List<ProjectDTO>>(_unitOfWork.Projects.GetList());
        }

        public void Update(int id, ProjectDTO project)
        {
            _unitOfWork.Projects.Update(id, _mapper.Map<Project>(project));
        }

        public void Update(ProjectDTO project)
        {
            _unitOfWork.Projects.Update(_mapper.Map<Project>(project));
        }

        public async System.Threading.Tasks.Task UpdateAsync(int id, ProjectDTO item)
        {
            await _unitOfWork.Projects.UpdateAsync(id, _mapper.Map<Project>(item));
        }
    }
}
