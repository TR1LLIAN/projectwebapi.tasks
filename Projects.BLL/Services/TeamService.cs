﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Create(TeamDTO team)
        {
            _unitOfWork.Teams.Create(_mapper.Map<Team>(team));
        }

        public async System.Threading.Tasks.Task CreateAsync(TeamDTO item)
        {
            await _unitOfWork.Teams.CreateAsync(_mapper.Map<Team>(item));
            await _unitOfWork.SaveAsync();
        }

        public void Delete(int id)
        {
            _unitOfWork.Teams.Delete(id);
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            await _unitOfWork.Teams.DeleteAsync(id);
        }

        public TeamDTO GetById(int id)
        {
            return _mapper.Map<TeamDTO>(_unitOfWork.Teams.GetItemById(id));
        }

        public async Task<TeamDTO> GetByIdAsync(int id)
        {
            return _mapper.Map<TeamDTO>(await _unitOfWork.Teams.GetItemByIdAsync(id));
        }

        public async Task<List<TeamDTO>> GetListAsync()
        {
            return _mapper.Map<List<TeamDTO>>(await _unitOfWork.Teams.GetListAsync());
        }

        public List<TeamDTO> GetTeams()
        {
            return _mapper.Map<List<TeamDTO>>(_unitOfWork.Teams.GetList());
        }

        public void Update(int id, TeamDTO team)
        {
            _unitOfWork.Teams.Update(id, _mapper.Map<Team>(team));
        }

        public void Update(TeamDTO team)
        {
            _unitOfWork.Teams.Update(_mapper.Map<Team>(team));
        }

        public async System.Threading.Tasks.Task UpdateAsync(int id, TeamDTO item)
        {
            await _unitOfWork.Teams.UpdateAsync(id, _mapper.Map<Team>(item));
        }
    }
}
