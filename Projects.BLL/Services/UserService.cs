﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(UserDTO user)
        {
            if (user.TeamId != null)
                _ = _unitOfWork.Teams.GetItemById((int)user.TeamId) ?? throw new ArgumentException($"Team with Id {user.TeamId} doesn't exist!");

            _unitOfWork.Users.Create(_mapper.Map<User>(user));
            _unitOfWork.Save();
        }

        public async System.Threading.Tasks.Task CreateAsync(UserDTO user)
        {
            if (user.TeamId != null)
                _ = _unitOfWork.Teams.GetItemById((int)user.TeamId) ?? throw new ArgumentException($"Team with Id {user.TeamId} doesn't exist!");

            await _unitOfWork.Users.CreateAsync(_mapper.Map<User>(user));
            await _unitOfWork.SaveAsync();
        }

        public void Delete(int id)
        {
            _ = _unitOfWork.Users.GetItemById(id) ?? throw new ArgumentNullException($"User with ID {id} not found!");
            _unitOfWork.Users.Delete(id);
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            _ = await _unitOfWork.Users.GetItemByIdAsync(id) ?? throw new ArgumentNullException($"User with ID {id} not found!");
            _unitOfWork.Users.Delete(id);
        }

        public UserDTO GetById(int id)
        {
            return _mapper.Map<UserDTO>(_unitOfWork.Users.GetItemById(id) ?? throw new ArgumentNullException($"ID {id} not found!"));
        }

        public async Task<UserDTO> GetByIdAsync(int id)
        {
            return _mapper.Map<UserDTO>(await _unitOfWork.Users.GetItemByIdAsync(id) ?? throw new ArgumentNullException($"ID {id} not found!"));
        }

        public async Task<List<UserDTO>> GetListAsync()
        {
            return _mapper.Map<List<UserDTO>>(await _unitOfWork.Users.GetListAsync());
        }

        public List<UserDTO> GetUsers()
        {
            return _mapper.Map<List<UserDTO>>(_unitOfWork.Users.GetList());
        }

        public async Task<List<UserDTO>> GetUsersAsync()
        {
            return _mapper.Map<List<UserDTO>>(await _unitOfWork.Users.GetListAsync());
        }

        public void Update(int id, UserDTO user)
        {
            _ = _unitOfWork.Users.GetItemById(id) ?? throw new ArgumentNullException($"User with ID {id} not found!");
            _unitOfWork.Users.Update(id, _mapper.Map<User>(user));
        }

        public void Update(UserDTO user)
        {
            _unitOfWork.Users.Update(_mapper.Map<User>(user));
        }

        public async System.Threading.Tasks.Task UpdateAsync(int id, UserDTO user)
        {
            _ = await _unitOfWork.Users.GetItemByIdAsync(id) ?? throw new ArgumentNullException($"User with ID {id} not found!");
            _unitOfWork.Users.Update(id, _mapper.Map<User>(user));
        }
    }
}
