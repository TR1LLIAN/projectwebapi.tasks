﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.Common.DTOmodels;
using Projects.DAL.Interfaces;
using Projects.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(TaskDTO task)
        {
            _unitOfWork.Tasks.Create(_mapper.Map<Task>(task));
        }

        public void Delete(int id)
        {
            _unitOfWork.Tasks.Delete(id);
        }

        public TaskDTO GetById(int id)
        {
            return _mapper.Map<TaskDTO>(_unitOfWork.Tasks.GetItemById(id));
        }

        public List<TaskDTO> GetTasks()
        {
            return _mapper.Map<List<TaskDTO>>(_unitOfWork.Tasks.GetList());
        }

        public void Update(int id, TaskDTO task)
        {
            _unitOfWork.Tasks.Update(id, _mapper.Map<Task>(task));
        }

        public List<TaskDTO> GetUnfinishedTasks(int userId)
        {
            if (_unitOfWork.Users.GetItemById(userId) == null)
                throw new ArgumentException($"User with Id {userId} doesn't exist!");

            return _mapper.Map<List<TaskDTO>>(_unitOfWork.Tasks.GetList().Where(t => t.PerformerId == userId && t.FinishedAt == null).ToList());
        }

        public void Update(TaskDTO task)
        {
            _unitOfWork.Tasks.Update(_mapper.Map<Task>(task));
        }

        public async System.Threading.Tasks.Task DeleteAsync(int id)
        {
            await _unitOfWork.Tasks.DeleteAsync(id);
        }

        public async System.Threading.Tasks.Task CreateAsync(TaskDTO item)
        {
            await _unitOfWork.Tasks.CreateAsync(_mapper.Map<Task>(item));
        }

        public async System.Threading.Tasks.Task UpdateAsync(int id, TaskDTO item)
        {
            await _unitOfWork.Tasks.UpdateAsync(id, _mapper.Map<Task>(item));
        }

        public async System.Threading.Tasks.Task<TaskDTO> GetByIdAsync(int id)
        {
            return _mapper.Map<TaskDTO>(await _unitOfWork.Tasks.GetItemByIdAsync(id));
        }

        public async System.Threading.Tasks.Task<List<TaskDTO>> GetListAsync()
        {
            return _mapper.Map<List<TaskDTO>>(await _unitOfWork.Tasks.GetListAsync());
        }
    }
}
