﻿using AutoMapper;
using Projects.BLL.Maping;
using System;

namespace Projects.BLL.Tests
{
    public class ServiceFixture : IDisposable
    {
        public ServiceFixture()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapProject>();
                cfg.AddProfile<MapTask>();
                cfg.AddProfile<MapTeam>();
                cfg.AddProfile<MapUser>();
            });

            GetMapper = config.CreateMapper();

        }

        public IMapper GetMapper { get; }

        public void Dispose()
        {

        }
    }
}
